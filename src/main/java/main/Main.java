package main;

import dao.PersonDao;
import dao.SetupDao;
import model.Address;
import model.Person;
import model.Phone;
import util.JpaUtil;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        try {
            new Main().run();
        } finally {
            JpaUtil.closeFactory();
        }
    }

    private void run() {
        new SetupDao().createSchema();

        PersonDao dao = new PersonDao();

        System.out.println(dao.findAllPersons());
        dao.insertPerson("Jill");
        dao.insertPerson("Jack");
        Person person = dao.findPersonByName("Jack");
        person.setName("Jackson");
        person.setAddress(new Address("Oak Street 1"));
        person.getPhones().addAll(Arrays.asList(new Phone("123"), new Phone("456") ));
        dao.savePerson(person);
        System.out.println(dao.findAllPersons());

    }



}
